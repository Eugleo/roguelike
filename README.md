# Racket roguelike

In my new project (so new in fact it doesn't even have a proper name yet) I try to
build a [roguelike][1] in [Racket][2]. Racket is a modern and cool lisp dialect and
I chose it because I want to get involved in functional programming, but at the same 
time don't want to struggle with Haskell.

## The plan

I'm following the [Python 3 libtcod tutorial][3], but without libtcod 
and without Python (yeah). I'll try to stick to the original where possible,
if not with the architecture, then at least with the general idea.

I'll also do a blog series about this, I think, in the future. 

## The motivation

The biggest motivation for me was the r/roguelikedev's 
[r/roguelikedev does the complete roguelike tutorial][4], which is an annual
event aimed at new (roguelike) developers (like me), provides them motivation
help, and a sense of community. If you're even only remotely interested in
making your own roguelike, be sure to visit that subreddit!


[1]: https://en.wikipedia.org/wiki/Roguelike
[2]: https://racket-lang.org
[3]: http://rogueliketutorials.com/libtcod/1
[4]: https://www.reddit.com/r/roguelikedev/comments/8s5x5n/roguelikedev_does_the_complete_roguelike_tutorial/
